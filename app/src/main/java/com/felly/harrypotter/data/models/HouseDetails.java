package com.felly.harrypotter.data.models;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class HouseDetails extends House implements Serializable {

    private List<MyCharacter> members;

    public HouseDetails(String _id, String name, String mascot, String headOfHouse, String houseGhost, String founder, String school, List<MyCharacter> members) {
        super(_id, name, mascot, headOfHouse, houseGhost, founder, school);
        this.members = members;
    }
    public List<MyCharacter> getMembers() {
        return members;
    }
    public void setMembers(List<MyCharacter> members) {
        this.members = members;
    }

    @NotNull
    @Override
    public String toString() {
        return "HouseDetails{" +
                "members=" + members +
                '}';
    }
}
