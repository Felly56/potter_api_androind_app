package com.felly.harrypotter.data.models;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class Spell {

    private String _id;
    @SerializedName("spell")
    private String spellName;
    @SerializedName("type")
    private String spellType;
    private String effect;

    public Spell(String _id, String spellName, String spellType, String effect) {
        this._id = _id;
        this.spellName = spellName;
        this.spellType = spellType;
        this.effect = effect;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSpellName() {
        return spellName;
    }

    public void setSpellName(String spellName) {
        this.spellName = spellName;
    }

    public String getSpellType() {
        return spellType;
    }

    public void setSpellType(String spellType) {
        this.spellType = spellType;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    @NotNull
    @Override
    public String toString() {
        return "Spell{" +
                "_id='" + _id + '\'' +
                ", spellName='" + spellName + '\'' +
                ", spellType='" + spellType + '\'' +
                ", effect='" + effect + '\'' +
                '}';
    }
}
