package com.felly.harrypotter.data.api;

import com.felly.harrypotter.data.models.House;
import com.felly.harrypotter.data.models.HouseDetails;
import com.felly.harrypotter.data.models.MyCharacter;
import com.felly.harrypotter.data.models.Spell;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PotterApi
{
    @GET("houses")
    Flowable<List<House>> getHouses(@Query("key") String key);

    @GET("houses/{id}")
    Flowable<List<HouseDetails>> getHouseById(@Path("id") String id, @Query("key") String key);

    @GET("characters")
    Flowable<List<MyCharacter>> getCharacters(@Query("key") String key);

    @GET("characters/{id}")
    Flowable<MyCharacter> getCharacterById(@Path("id") String id, @Query("key") String key);

    @GET("spells")
    Flowable<List<Spell>> getSpells(@Query("key") String key);

}
