package com.felly.harrypotter.di.modules;

import androidx.lifecycle.ViewModel;


import com.felly.harrypotter.di.viewmodel.ViewModelKey;
import com.felly.harrypotter.ui.characters.CharactersFragmentViewModel;
import com.felly.harrypotter.ui.houses.HouseFragmentViewModel;
import com.felly.harrypotter.ui.spell.SpellsFragmentViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainViewModelsModule
{
    @Binds
    @IntoMap
    @ViewModelKey(HouseFragmentViewModel.class)
    public abstract ViewModel bindHouseFragmentViewModel(HouseFragmentViewModel viewModel);


    @Binds
    @IntoMap
    @ViewModelKey(CharactersFragmentViewModel.class)
    public abstract ViewModel binCharactersFragmentViewModel(CharactersFragmentViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SpellsFragmentViewModel.class)
    public abstract ViewModel bindSpellsFragmentViewModel(SpellsFragmentViewModel viewModel);
}
