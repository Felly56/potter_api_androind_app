package com.felly.harrypotter.di.modules;

import com.felly.harrypotter.data.api.PotterApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class MainActivityModule {

    @Provides
    static PotterApi provideApi(Retrofit retrofit){
        return retrofit.create(PotterApi.class);
    }
}
