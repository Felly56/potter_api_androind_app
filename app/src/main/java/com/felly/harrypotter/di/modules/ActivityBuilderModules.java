package com.felly.harrypotter.di.modules;

import com.felly.harrypotter.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModules
{
    @ContributesAndroidInjector
    (
        modules =
        {
            MainActivityModule.class,
            MainFragmentsBuilderModules.class,
            MainViewModelsModule.class,
        }
    )
    abstract MainActivity contributeMainActivity();
}
