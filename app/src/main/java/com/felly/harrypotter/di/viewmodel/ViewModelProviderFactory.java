package com.felly.harrypotter.di.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

public class ViewModelProviderFactory implements ViewModelProvider.Factory
{
    private Map<Class<? extends ViewModel>, Provider<ViewModel>> creators;

    @Inject
    public ViewModelProviderFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> creators)
    {
        this.creators = creators;
    }
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass)
    {
        Provider<? extends ViewModel> creator = creators.get(modelClass);
        if(creator == null)//if the view model hasn't been created.
        {
            //loop through to look for classes that have @ViewModelKey
            for(Map.Entry<Class<? extends ViewModel>, Provider<ViewModel>> entry : creators.entrySet())
            {
                // if it's allowed, set the Provider<ViewModel>
                if(modelClass.isAssignableFrom(entry.getKey()))
                {
                    creator = entry.getValue();
                    break;
                }
            }
        }
        if(creator == null)//if this is not one of the allowed keys, throw an exception
        {
            throw new IllegalArgumentException("Unknown class model"+ modelClass);
        }
        try
        {
            return (T) creator.get();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

}
