package com.felly.harrypotter.di.modules;

import com.felly.harrypotter.ui.houses.HousesAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class HousesFragmentModule {
    @Provides
    static HousesAdapter provideHousesAdapter(){
        return new HousesAdapter();
    }
}
