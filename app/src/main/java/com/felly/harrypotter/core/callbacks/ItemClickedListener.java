package com.felly.harrypotter.core.callbacks;

public interface ItemClickedListener<T> {
    void onItemClicked(T item);
}
