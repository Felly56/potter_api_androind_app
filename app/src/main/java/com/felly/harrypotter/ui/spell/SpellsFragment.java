package com.felly.harrypotter.ui.spell;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.felly.harrypotter.R;
import com.felly.harrypotter.di.viewmodel.ViewModelProviderFactory;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpellsFragment extends DaggerFragment {

    private static final String TAG = "SpellsFragment";
    @Inject
    ViewModelProviderFactory viewModelProvider;

    private SpellsFragmentViewModel viewModel;

    private RecyclerView spellsRecycleView;

    private SpellsListAdapter adapter;

    private RelativeLayout progressBarLayout;
    private ProgressBar progressBar;
    private TextView errorTextView;
    public SpellsFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_spells, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getActivity() != null){
            BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.bottom_nav);
            bottomNavigationView.getMenu().getItem(3).setChecked(true);
        }
        viewModel = new ViewModelProvider(this, viewModelProvider).get(SpellsFragmentViewModel.class);
        initViews(view);
        observeHousesData();
        viewModel.requestSpells();
    }
    private void initViews(View view){
        setupNavController(view);
        spellsRecycleView = view.findViewById(R.id.spells_recycle_view);
        progressBarLayout = view.findViewById(R.id.progress_bar_layout);
        errorTextView = view.findViewById(R.id.errorText);
        progressBar = view.findViewById(R.id.progressBar);

        adapter = new SpellsListAdapter();
        spellsRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        spellsRecycleView.setAdapter(adapter);

    }
    private void setupNavController(View view){
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        NavController navController =  Navigation.findNavController(view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);
    }

    private void observeHousesData(){
        viewModel.getAllSpells().removeObservers(getViewLifecycleOwner());
        viewModel.getAllSpells().observe(this, listDataResources -> {
            if (listDataResources != null)
            {
                switch (listDataResources.status)
                {
                    case LOADING:
                        showProgressBar(false, null);
                        break;
                    case ERROR:
                        showProgressBar(true, listDataResources.message);
                        break;
                    case SUCCESS:
                        if(listDataResources.data != null && listDataResources.data.size() > 0)
                        {
                            showProgressBar(true, "success");
                            adapter.setSpellList(listDataResources.data);
                        }
                        else
                            showProgressBar(true, "No data available");
                        break;
                }
            }
            else{
                showProgressBar(true, "Failed to connect");
            }
        });
    }
    private void showProgressBar(boolean isShowing, String message){

        if(isShowing){

            if(message.equals("success"))
            {
                progressBarLayout.setVisibility(View.GONE);
                errorTextView.setVisibility(View.GONE);
                spellsRecycleView.setVisibility(View.VISIBLE);
            }
            else
            {
                spellsRecycleView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                errorTextView.setText(message);
                errorTextView.setVisibility(View.VISIBLE);
            }
        }
        else{
            spellsRecycleView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.GONE);
            progressBarLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

        }
    }

}
