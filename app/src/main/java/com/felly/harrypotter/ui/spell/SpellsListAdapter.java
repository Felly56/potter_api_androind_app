package com.felly.harrypotter.ui.spell;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.felly.harrypotter.R;
import com.felly.harrypotter.data.models.Spell;

import java.util.ArrayList;
import java.util.List;

public class SpellsListAdapter extends RecyclerView.Adapter<SpellsListAdapter.SpellAdapterViewHolder> {

    private List<Spell> spellList = new ArrayList<>();
    private static final String TAG = "SpellsListAdapter";
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setSpellList(List<Spell> spellList){
        if(this.spellList.size() > 0)
            this.spellList.clear();

        this.spellList.addAll(spellList);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public SpellAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SpellAdapterViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.spell_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull SpellAdapterViewHolder holder, int position) {
        holder.bind(spellList.get(position));
    }

    @Override
    public int getItemCount() {
        return spellList.size();
    }

    class SpellAdapterViewHolder extends RecyclerView.ViewHolder
    {
        private TextView spellName, spellType, spellEffects;
        public SpellAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            spellName = itemView.findViewById(R.id.spellName);
            spellType = itemView.findViewById(R.id.type);
            spellEffects = itemView.findViewById(R.id.effects);
        }
        private void bind(Spell spell){
            Log.d(TAG, "bind: Spell: "+spell);
            spellName.setText(spell.getSpellName());
            spellType.setText(spell.getSpellType());
            spellEffects.setText(spell.getEffect());
        }

    }
}
