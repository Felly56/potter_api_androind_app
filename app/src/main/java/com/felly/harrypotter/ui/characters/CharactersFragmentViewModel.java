package com.felly.harrypotter.ui.characters;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.felly.harrypotter.core.utils.Constants;
import com.felly.harrypotter.core.utils.DataResources;
import com.felly.harrypotter.data.api.PotterApi;
import com.felly.harrypotter.data.models.HouseDetails;
import com.felly.harrypotter.data.models.MyCharacter;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class CharactersFragmentViewModel extends ViewModel
{
    private PotterApi api;

    private MediatorLiveData<DataResources<List<HouseDetails>>> houseInfoResource = new MediatorLiveData<>();
    private MediatorLiveData<DataResources<List<MyCharacter>>> characterResources = new MediatorLiveData<>();
    private MediatorLiveData<DataResources<MyCharacter>> singleCharacterResources = new MediatorLiveData<>();

    @Inject
    public CharactersFragmentViewModel(PotterApi api)
    {
        this.api = api;
    }
//

    public void requestHouseInfo(String id){
        houseInfoResource.setValue(DataResources.loading("Loading, Please Wait..."));
        final LiveData<DataResources<List<HouseDetails>>> source = LiveDataReactiveStreams.fromPublisher(
                api.getHouseById(id,Constants.API_KEY).onErrorReturn(throwable -> Collections.singletonList(new HouseDetails("error","","","","","","", null))
                        )
                        .map((Function<List<HouseDetails>, DataResources<List<HouseDetails>>>) house -> {
                            if(house.get(0).get_id().equals(Constants.ERROR))
                                return DataResources.error(null,"Failed to connect. Please try again");
                            return DataResources.success(house);
                        }).subscribeOn(Schedulers.io())
        );
        //Removing MediatorLiveData Source
        houseInfoResource.addSource(source, houseDataResources -> {
            houseInfoResource.setValue(houseDataResources);
            houseInfoResource.removeSource(source);
        });

    }
    public LiveData<DataResources<List<HouseDetails>>> getHouseInfo(){
        return houseInfoResource;
    }

    /********
     *
     */
    public void requestAllCharacters(){
        characterResources.setValue(DataResources.loading("Loading, Please Wait..."));
        final LiveData<DataResources<List<MyCharacter>>> source = LiveDataReactiveStreams.fromPublisher(
            api.getCharacters(Constants.API_KEY)
                .onErrorReturn(throwable -> Collections.singletonList(new MyCharacter("error","","","","","","")))
                .map((Function<List<MyCharacter>, DataResources<List<MyCharacter>>>) characters -> {
                    if(characters.get(0).get_id().equals(Constants.ERROR))
                        return DataResources.error(null,"Failed to connect. Please try again");
                    return DataResources.success(characters);
                })
                .subscribeOn(Schedulers.io())
        );
        characterResources.addSource(source, characters -> {
            characterResources.setValue(characters);
            characterResources.removeSource(source);
        });
    }
    public LiveData<DataResources<List<MyCharacter>>> getAllCharacters(){
        return characterResources;
    }
//
    /********
     *
     */
    public void requestSingleCharacter(String id){
        singleCharacterResources.setValue(DataResources.loading("Loading, Please Wait..."));
        final LiveData<DataResources<MyCharacter>> source = LiveDataReactiveStreams.fromPublisher(
                api.getCharacterById(id, Constants.API_KEY).
                        onErrorReturn(throwable -> new MyCharacter("error","","","","","","")).
                        map((Function<MyCharacter, DataResources<MyCharacter>>) character -> {
                            if(character.get_id().equals(Constants.ERROR))
                                return DataResources.error(null,"Failed to connect. Please try again");
                            return DataResources.success(character);
                        })
                        .subscribeOn(Schedulers.io())
        );
        singleCharacterResources.addSource(source, character -> {
            singleCharacterResources.setValue(character);
            singleCharacterResources.removeSource(source);
        });
    }
    public LiveData<DataResources<MyCharacter>> getSingleCharacter(){
        return singleCharacterResources;
    }
//
}
