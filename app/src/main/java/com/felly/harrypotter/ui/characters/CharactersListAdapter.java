package com.felly.harrypotter.ui.characters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.felly.harrypotter.R;
import com.felly.harrypotter.core.callbacks.ItemClickedListener;
import com.felly.harrypotter.core.utils.Constants;
import com.felly.harrypotter.data.models.MyCharacter;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.ArrayList;
import java.util.List;

public class CharactersListAdapter extends RecyclerView.Adapter<CharactersListAdapter.CharactersListViewHolder>
{
    private List<MyCharacter> characterList = new ArrayList<>();
    private ItemClickedListener<MyCharacter> listener;

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setCharacterList(List<MyCharacter> characterList){
        if(this.characterList.size() > 0)
            this.characterList.clear();

        this.characterList.addAll(characterList);
        notifyDataSetChanged();
    }
    public void setItemClickedListener(ItemClickedListener<MyCharacter> listener)
    {
        this.listener = listener;
    }
    @NonNull
    @Override
    public CharactersListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CharactersListViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.character_item, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull CharactersListViewHolder holder, int position) {
        holder.bind(characterList.get(position));
    }

    @Override
    public int getItemCount() {
        return characterList.size();
    }
    class CharactersListViewHolder extends RecyclerView.ViewHolder
    {
        private ShapeableImageView characterImageView;
        private TextView characterName;
        public CharactersListViewHolder(@NonNull View itemView) {
            super(itemView);
        }
        private void bind(MyCharacter character){
            characterImageView = itemView.findViewById(R.id.character_icon);
            characterName = itemView.findViewById(R.id.character_name);

            characterName.setText(character.getName());
            setImage(character.getName().split(" ")[0]);

            itemView.setOnClickListener(view -> listener.onItemClicked(character));
        }
        private void setImage(String characterName)
        {
            Constants.loadImage(characterName, characterImageView);
        }
    }
}
