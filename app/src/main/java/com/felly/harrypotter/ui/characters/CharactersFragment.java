package com.felly.harrypotter.ui.characters;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.felly.harrypotter.R;
import com.felly.harrypotter.core.callbacks.ItemClickedListener;
import com.felly.harrypotter.core.utils.Constants;
import com.felly.harrypotter.core.utils.DataResources;
import com.felly.harrypotter.data.models.House;
import com.felly.harrypotter.data.models.MyCharacter;
import com.felly.harrypotter.di.viewmodel.ViewModelProviderFactory;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
/**
 * A simple {@link Fragment} subclass.
 */
public class CharactersFragment extends DaggerFragment implements ItemClickedListener<MyCharacter> {


    @Inject
    ViewModelProviderFactory viewModelProvider;

    private RecyclerView gridView;
    private CharactersListAdapter adapter;


    private CharactersFragmentViewModel viewModel;
    private RelativeLayout progressBarLayout;
    private ProgressBar progressBar;
    private TextView errorTextView,houseName, headOfHouse,ghostOfHouse,houseFounder;


    private ShapeableImageView house_icon;


    /***Character dialog views**/
    private Dialog characterDetailsDialog;
    private ShapeableImageView characterIcon;
    private TextView characterName,role,house,bloodStatus,species, fragmentDescriptionText;
    private LinearLayout characterDialogInfoLayout;

    private RelativeLayout dialogProgressBarParent, fragmentDescriptionLayout, houseInfoLayout;

    private Bundle params;
    public CharactersFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_characters, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        params = getArguments();
        initViews(view);
        viewModel = new ViewModelProvider(this, viewModelProvider).get(CharactersFragmentViewModel.class);
        processSourceParams();

    }
    private void initViews(View view){
        setupNavController(view);
        gridView = view.findViewById(R.id.characters_gridView);
        houseName = view.findViewById(R.id.houseName);
        headOfHouse = view.findViewById(R.id.head_of_house);
        ghostOfHouse = view.findViewById(R.id.house_ghost);
        houseFounder = view.findViewById(R.id.house_founder);
        house_icon = view.findViewById(R.id.house_icon);

        progressBarLayout = view.findViewById(R.id.progress_bar_layout);
        progressBar = view.findViewById(R.id.progressBar);
        houseInfoLayout = view.findViewById(R.id.house_info_layout);
        fragmentDescriptionLayout = view.findViewById(R.id.fragment_description_txt_layout);
        fragmentDescriptionText = view.findViewById(R.id.fragment_description_text);


        errorTextView = view.findViewById(R.id.errorText);
        gridView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapter = new CharactersListAdapter();
        gridView.setAdapter(adapter);
        adapter.setItemClickedListener(this);
        setupCharacterDialog();
    }
    private void setupNavController(View view){

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        if(params.containsKey(Constants.HOUSE_EXTRA))
        {
            selectNavBarMenuItem(2);
            toolbar.setTitle("Characters");
        }
        else if (params.containsKey(Constants.STUDENTS_EXTRA))
        {
            selectNavBarMenuItem(1);
            toolbar.setTitle("Students");
        }
        else if (params.containsKey(Constants.CHARACTERS_EXTRA))
        {
            selectNavBarMenuItem(2);
            toolbar.setTitle("Characters");
        }

        CollapsingToolbarLayout layout = view.findViewById(R.id.top_bar_layout);
        NavController navController =  Navigation.findNavController(view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupWithNavController(layout, toolbar, navController, appBarConfiguration);
    }
    private void selectNavBarMenuItem(int index){
        if(getActivity() != null){
            BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.bottom_nav);
            bottomNavigationView.getMenu().getItem(index).setChecked(true);
        }
    }
    private void processSourceParams()
    {
        if(params != null)
        {
            if(params.containsKey(Constants.HOUSE_EXTRA)){
                observeHouseData();
                House house = (House) params.getSerializable(Constants.HOUSE_EXTRA);
                setHouseInfo(house);
                viewModel.requestHouseInfo(house.get_id());
            }
            else
            {
                houseInfoLayout.setVisibility(View.GONE);
                fragmentDescriptionLayout.setVisibility(View.VISIBLE);
                if (params.containsKey(Constants.STUDENTS_EXTRA))
                {
                    fragmentDescriptionText.setText(R.string.list_of_all_students);
                    observeCharactersData(Constants.STUDENTS_EXTRA);
                }
                else
                {
                    fragmentDescriptionText.setText(R.string.list_of_all_characters);
                    observeCharactersData(Constants.CHARACTERS_EXTRA);
                }
                viewModel.requestAllCharacters();
            }

        }
    }
    private void observeHouseData(){
        viewModel.getHouseInfo().removeObservers(getViewLifecycleOwner());
        viewModel.getHouseInfo().observe(this, houseDataResources -> {
            if(houseDataResources != null){
                switch (houseDataResources.status){
                    case LOADING:
                        showProgressBar(false, null);
                        break;
                    case ERROR:
                        showProgressBar(true, houseDataResources.message);
                        break;
                    case SUCCESS:
                        if(houseDataResources.data != null && houseDataResources.data.size() > 0){
                            adapter.setCharacterList(houseDataResources.data.get(0).getMembers());
                            showProgressBar(true, "success");
                        }
                        else
                            showProgressBar(true, "No data available");
                        break;
                }
            }
        });
    }
    private void observeCharactersData(String entity){
        viewModel.getAllCharacters().removeObservers(getViewLifecycleOwner());
        viewModel.getAllCharacters().observe(this, charactersResourcesData -> {
            if(charactersResourcesData != null){
                switch (charactersResourcesData.status){
                    case LOADING:
                        showProgressBar(false, null);
                    break;
                    case ERROR:
                        showProgressBar(true, charactersResourcesData.message);
                    break;
                    case SUCCESS:
                       if(charactersResourcesData.data != null && charactersResourcesData.data.size() >0){
                           showProgressBar(true, "success");
                           if(entity.equals(Constants.STUDENTS_EXTRA)){
                               List<MyCharacter> students = charactersResourcesData.data.stream()
                                       .filter(character -> character.getRole() != null && character.getRole().equals(Constants.STUDENTS_EXTRA))
                                       .collect(Collectors.toList());
                               adapter.setCharacterList(students);
                           }
                           else
                               adapter.setCharacterList(charactersResourcesData.data);
                       }
                       else showProgressBar(true, "No data available");
                    break;
                }
            }
        });
    }
    private void setHouseInfo(House house){
        fragmentDescriptionLayout.setVisibility(View.GONE);
        houseInfoLayout.setVisibility(View.VISIBLE);
        houseName.setText(house.getName());
//        schoolName.setText(house.getSchool());
        headOfHouse.setText(house.getHeadOfHouse());
        ghostOfHouse.setText(house.getHouseGhost());
        houseFounder.setText(house.getFounder());

        house_icon.setImageResource(Constants.getHouseIncon(house.getName()));
    }

    private void setupCharacterDialog(){
        characterDetailsDialog = new Dialog(getContext());
        characterDetailsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        characterDetailsDialog.setContentView(R.layout.character_detials_dialog);
        characterDetailsDialog.setCanceledOnTouchOutside(false);
        characterDetailsDialog.setCancelable(false);

        characterIcon = characterDetailsDialog.findViewById(R.id.character_icon);
        characterName = characterDetailsDialog.findViewById(R.id.characterName);
        role = characterDetailsDialog.findViewById(R.id.role);
        house = characterDetailsDialog.findViewById(R.id.house);
        bloodStatus = characterDetailsDialog.findViewById(R.id.bloodStatus);
        species = characterDetailsDialog.findViewById(R.id.species);
        characterDialogInfoLayout = characterDetailsDialog.findViewById(R.id.characterDialogInfoLayout);
        dialogProgressBarParent = characterDetailsDialog.findViewById(R.id.dialogProgressBarParent);

        characterDetailsDialog.findViewById(R.id.close_dialog).setOnClickListener(view -> characterDetailsDialog.dismiss());

        if (characterDetailsDialog.getWindow() != null) {
            characterDetailsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            characterDetailsDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        }
    }
    @Override
    public void onItemClicked(MyCharacter character) {
            loadCharacterInfo(character);
    }
    private void loadCharacterInfo(MyCharacter character){

        Constants.loadImage(character.getName().split(" ")[0], characterIcon);
        if(!getActivity().isFinishing()){
            characterDetailsDialog.show();
        }
        if(character.getRole() != null){
            setDialogDetails(character);
        }
        else{
            viewModel.requestSingleCharacter(character.get_id());
            viewModel.getSingleCharacter().removeObservers(getViewLifecycleOwner());
            viewModel.getSingleCharacter().observe(this, dataResource ->{
                switch (dataResource.status){
                    case LOADING:
                        characterDialogInfoLayout.setVisibility(View.GONE);
                        dialogProgressBarParent.setVisibility(View.VISIBLE);
                        break;
                    case ERROR:
//                   characterDialogInfoLayout.setVisibility(View.GONE);
                        dialogProgressBarParent.setVisibility(View.GONE);
                        break;
                    case SUCCESS:
                        setDialogDetails(dataResource.data);
                        characterDialogInfoLayout.setVisibility(View.VISIBLE);
                        dialogProgressBarParent.setVisibility(View.GONE);
                        break;
                }
            });
        }
    }
    private void showProgressBar(boolean isShowing, String message){

        if(isShowing){

            if(message.equals("success"))
            {
                progressBarLayout.setVisibility(View.GONE);
                errorTextView.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
            }
            else
            {
                gridView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                errorTextView.setText(message);
                errorTextView.setVisibility(View.VISIBLE);
            }
        }
        else{
            gridView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.GONE);
            progressBarLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

        }
    }
    private void setDialogDetails(MyCharacter character){
        characterName.setText(character.getName());
        role.setText(character.getRole());
        house.setText(character.getHouse());
        bloodStatus.setText(character.getBloodStatus());
        species.setText(character.getSpecies());
    }
}
